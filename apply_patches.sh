#!/bin/bash

# Variables
name="ModemManager"
version="1.14.0.pinephone"
# Script
tar -xzvf ${name}-*.tar.gz
original_folder=$(ls -d ${name}-* | grep -v "tar.gz")
mv $original_folder ${name}-${version}
cd ${name}-${version}
for i in $(find debian/patches/ -name "*.patch"); do patch -p1 < $i; done
cd ..
tar -czvf ${name}-${version}.tar.gz ${name}-${version}
